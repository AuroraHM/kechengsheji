package SportsManager;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DeleteFrame extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textField;

    /**
     * Create the frame.
     */
    public DeleteFrame(String drv, String url, String usr, String pwd) {
        setTitle("\u5220\u9664\u4FE1\u606F");//ɾ����Ϣ
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(500, 200, 451, 122);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);

        textField = new JTextField();
        textField.setBounds(127, 28, 135, 24);
        textField.setColumns(10);

        JButton button = new JButton("\u5220\u9664");//ɾ��
        button.setBounds(293, 26, 72, 27);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String a = textField.getText();
                new Delete(drv, url, usr, pwd, a);
            }
        });
        contentPane.setLayout(null);
        button.setForeground(Color.RED);
        button.setFont(new Font("΢���ź�", Font.BOLD, 15));
        contentPane.add(button);
        contentPane.add(textField);

        JLabel label = new JLabel("ѧ��");
        label.setFont(new Font("����", Font.BOLD, 15));
        label.setBounds(63, 31, 48, 18);
        contentPane.add(label);
    }

}
