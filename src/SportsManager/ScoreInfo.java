package SportsManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

class ScoreInfo extends JFrame {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    JTable table = new JTable();
    private Connection conn;

    public ScoreInfo(String drv, String url, String usr, String pwd, String tb) {
        this.setBounds(500, 300, 800, 200);
        this.setTitle("�ɼ���Ϣ");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        try {
            Class.forName(drv);
            this.conn = DriverManager.getConnection(url, usr, pwd);
            table = query(tb);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.getContentPane().add(new JScrollPane(table));
        this.setVisible(true);
        try {
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JTable query(String table) throws SQLException {
        DefaultTableModel tbmode = new DefaultTableModel();
        String sql = "SELECT * FROM " + table + ";";
        try {
            Statement Stmt = conn.createStatement();
            ResultSet rs = Stmt.executeQuery(sql);
            ResultSetMetaData meta = rs.getMetaData();
            int colcount = meta.getColumnCount();
            for (int i = 1; i <= colcount; i++)
                tbmode.addColumn(meta.getColumnName(i));
            Object[] col = new Object[colcount];
            while (rs.next()) {
                for (int j = 1; j <= col.length; j++)
                    col[j - 1] = rs.getString(j);
                tbmode.addRow(col);
            }
            rs.close();
            Stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JTable(tbmode);
    }
}

