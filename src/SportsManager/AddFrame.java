package SportsManager;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddFrame extends JFrame {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;//内容窗格
    private JTextField textField;//文本域
    private JTextField textField_1;//文本字段1
    private JTextField textField_2;
    private JTextField textField_3;
    private JTextField textField_4;
    private JTextField textField_5;
    private JTextField textField_6;
    private JTextField textField_7;
    JButton btnNewButton;

    /**
     * Create the frame.
     */
    public AddFrame(String drv, String url, String usr, String pwd) {
        setTitle("添加参赛学生信息");//添加学生信息
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(500, 200, 450, 528);
        contentPane = new JPanel();   //创建新的内容窗格
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);

        JLabel label = new JLabel("学号");//学号
        label.setBounds(69, 40, 90, 20);
        label.setFont(new Font("微软雅黑", Font.BOLD, 15));

        JLabel label_1 = new JLabel("姓名");//姓名
        label_1.setBounds(69, 82, 90, 20);
        label_1.setFont(new Font("微软雅黑", Font.BOLD, 15));

        JLabel label_2 = new JLabel("性别");//性别
        label_2.setBounds(69, 124, 90, 20);
        label_2.setFont(new Font("微软雅黑", Font.BOLD, 15));

        JLabel label_3 = new JLabel("参赛项目");//参赛项目
        label_3.setBounds(69, 166, 60, 20);
        label_3.setFont(new Font("微软雅黑", Font.BOLD, 15));

        JLabel label_4 = new JLabel("\u7C4D\u8D2F");//籍贯
        label_4.setBounds(69, 208, 30, 20);
        label_4.setFont(new Font("微软雅黑", Font.BOLD, 15));

        JLabel label_5 = new JLabel("班号");//班级
        label_5.setBounds(69, 250, 30, 20);
        label_5.setFont(new Font("微软雅黑", Font.BOLD, 15));

        JLabel label_6 = new JLabel("\u4E13\u4E1A");//专业
        label_6.setBounds(69, 292, 30, 20);
        label_6.setFont(new Font("微软雅黑", Font.BOLD, 15));

        JLabel label_7 = new JLabel("系");//系
        label_7.setBounds(69, 334, 15, 20);
        label_7.setFont(new Font("微软雅黑", Font.BOLD, 15));

        btnNewButton = new JButton("添加");//添加
        btnNewButton.setBounds(175, 418, 74, 29);

        btnNewButton.setForeground(Color.BLACK);
        btnNewButton.setFont(new Font("微软雅黑", Font.BOLD, 15));

        textField = new JTextField();
        textField.setBounds(219, 39, 153, 24);
        textField.setColumns(10);

        textField_1 = new JTextField();
        textField_1.setBounds(219, 81, 153, 24);
        textField_1.setColumns(10);

        textField_2 = new JTextField();
        textField_2.setBounds(219, 123, 153, 24);
        textField_2.setColumns(10);

        textField_3 = new JTextField();
        textField_3.setBounds(219, 165, 153, 24);
        textField_3.setColumns(10);

        textField_4 = new JTextField();
        textField_4.setBounds(219, 207, 153, 24);
        textField_4.setColumns(10);

        textField_5 = new JTextField();
        textField_5.setBounds(219, 249, 153, 24);
        textField_5.setColumns(10);

        textField_6 = new JTextField();
        textField_6.setBounds(219, 291, 153, 24);
        textField_6.setColumns(10);

        textField_7 = new JTextField();
        textField_7.setBounds(219, 333, 153, 24);
        textField_7.setColumns(10);

        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == btnNewButton) {
                    String a = textField.getText();
                    String b = textField_1.getText();
                    String c = textField_2.getText();
                    String d = textField_3.getText();
                    String i = textField_4.getText();
                    String f = textField_5.getText();
                    String g = textField_6.getText();
                    String h = textField_7.getText();
                    new Add(a, b, d, c, i, f, g, h, drv, url, usr, pwd);
                }
            }
        });
        contentPane.setLayout(null);
        contentPane.add(label);
        contentPane.add(label_1);
        contentPane.add(label_2);
        contentPane.add(label_4);
        contentPane.add(label_3);
        contentPane.add(label_5);
        contentPane.add(label_6);
        contentPane.add(label_7);
        contentPane.add(textField_7);
        contentPane.add(textField_6);
        contentPane.add(textField_5);
        contentPane.add(textField_4);
        contentPane.add(textField_3);
        contentPane.add(textField);
        contentPane.add(textField_1);
        contentPane.add(textField_2);
        contentPane.add(btnNewButton);
    }
}

