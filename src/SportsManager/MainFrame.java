package SportsManager;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainFrame extends JFrame {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setTitle("运动会管理系统");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(600, 300, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String url = "jdbc:sqlserver://127.0.0.1:1433;DatabaseName=运动会管理系统数据库";
		String userName = "sa";
		String pwd = "123456";
		String tab1 = "参赛学生基本信息";
		String tab2 = "比赛项目信息";
		String tab3 = "得分信息";

		JButton btnNewButton = new JButton("参赛学生信息查询");//学生信息系查询
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new StuInfo(driver, url, userName, pwd, tab1);
			}
		});
		btnNewButton.setFont(new Font("微软雅黑", Font.BOLD, 15));
		btnNewButton.setBounds(125, 98, 171, 27);
		contentPane.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("比赛项目查询");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ClassInfo(driver, url, userName, pwd, tab2);
			}
		});
		btnNewButton_1.setFont(new Font("微软雅黑", Font.BOLD, 15));
		btnNewButton_1.setBounds(135, 149, 146, 27);
		contentPane.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("得分查询");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ScoreInfo(driver, url, userName, pwd, tab3);
			}
		});
		btnNewButton_2.setFont(new Font("微软雅黑", Font.BOLD, 15));
		btnNewButton_2.setBounds(151, 199, 113, 27);
		contentPane.add(btnNewButton_2);

		JLabel label = new JLabel("欢迎来到运动会管理系统");
		label.setFont(new Font("微软雅黑", Font.BOLD, 25));
		label.setBounds(35, 40, 383, 43);
		contentPane.add(label);

		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("");
		menuBar.setBounds(0, 0, 432, 27);
		contentPane.add(menuBar);

		JMenu mnNewMenu = new JMenu("\u4FE1\u606F\u66F4\u65B0");//信息更新
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("\u5B66\u751F\u4FE1\u606F\u5F55\u5165");//学生信息录入
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddFrame af1 = new AddFrame(driver, url, userName, pwd);
				af1.setVisible(true);
			}
		});
		mnNewMenu.add(mntmNewMenuItem);

		JMenuItem menuItem = new JMenuItem("\u5B66\u751F\u4FE1\u606F\u5220\u9664");//学生信息删除
		menuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DeleteFrame df1 = new DeleteFrame(driver, url, userName, pwd);
				df1.setVisible(true);
			}
		});
		mnNewMenu.add(menuItem);
	}
}
