package pratices;

import java.sql.*;

public class Conn {
	static Connection con;

	public static void printDB(ResultSet res) {
		try {
			System.out.println("Sno\t\tSname\t\tSsex\t\tSage");
			while (res.next()) {
				String Sno = res.getString("Sno");//获取字段名为SNO的字段表
				String Sname = res.getString("Sname");
				String Ssex = res.getString("Ssex");
				String Sage = res.getString("Sage");
				System.out.println(Sno + "\t\t" + Sname + "\t\t" + Ssex + "\t\t" + Sage);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		try {//加载驱动
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			System.out.println("驱动加载成功");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			String url = "jdbc:sqlserver://localhost:1433;database = 运动会管理系统数据库";
			String user = "sa";//登录数据库时的用户名
			String password = "123456";//登录数据库时的密码
			con = DriverManager.getConnection(url, user, password);//数据源
			System.out.println("数据源加载成功");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Conn c = new Conn();
		Conn.con = c.getConnection();
	}

}
